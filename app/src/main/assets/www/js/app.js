var app = {

	// App Information
    id: '775322054',
    country: 'pr',
    os: 'ANDROID',
    device: 'android',
    version: null,
    build: null,
    uuid: null,
    connectionType:'',
    registrationId: null,
    rate: null,
    enableAppRate: true,
    enableCheckAppVersion: true,
    senderId: '680347282653',
    distanceMeasure: 'mi',
    sessionPasswordTime: 10,
    packageName:'com.todoclaro.miclaroapp',
    pushNotification: null,

    // Gateway Application ID
    gatewayAppId: 'At01bMi0aXhr6ktmTaow',

    apiKey: '6af3982a-ce65-41a0-93d9-52bd172685cd',

	// Debug mode
    //
    debug: true,

    /**** START URLs REBRANDING ****/
    // newUrl: 'http://wslogin2.claroinfo.com/api/', // QA
    newUrl: 'https://wsloginapp.claropr.com/api/', // PROD

    // processURL: 'https://us-central1-usf-crud-firebase.cloudfunctions.net/rebranding', // MOCK SERVER
    // processURL : 'http://rebranding.claroinfo.com/proccess/procesos-dev.aspx', // QA
    // processURL : 'https://rebranding.claropr.com/proccess/procesos-dev.aspx', // PRE PROD
    processURL: 'https://miclaro.claropr.com/proccess/procesos-dev.aspx', // PROD
    // processURL: 'http://miclaroreferals.claroinfo.com/proccess/procesos-dev.aspx', // REFER
    // Chat URL
    chatURL: 'https://chat3.claropr.com/webapiserver/ECSApp/ChatWidget3/ChatPanel.aspx',
    /**** END URLs REBRANDING ****/

    apiUrl: 'https://wsclarorprodnew.claropr.com/api-miclaro-services-prod-new/miclaro/', //Prod Active
    // Help URL
    helpURL: 'http://soporteapps.speedymovil.com:8090/appFeedback/service/feedback/application',

    // Application Data
    cache: {},
    session: {},

    // Application Containers
    collections: {},
    models: {},
    daos: {},
    views: {},
    utils: {},
    router: null,

    // File System
    fileSystem: null,

    //Google Analitycs
    gaPlugin: null,
    gaAccountID: 'UA-44057429-1',

    // Application boolena
    isApplication: true,

    // Loader
    spinner: null,

	//Menu
	isMenuOpen: false,

    timeoutID: null,

    // Application Constructor
    initialize: function() {

        //Debug mode
        if(!app.debug){
            console.log = console.info = console.error = console.warn = function(){};
        }

        // Bind events
        app.bindEvents();

        // Global utils
        app.utils.network = app.utils.Network;
        app.utils.browser = app.utils.Browser;
        app.utils.tools = app.utils.Tools;
        app.utils.loader = app.utils.Loader;

        // create $.mobile variable for web version
        if($.mobile===undefined){
        	$.mobile = {};
        	$.mobile.activePage = null;
        	$.mobile.changePage = function(){};
        }

        // Preload views
        app.TemplateManager.preload(function(){
            console.log('#preload complete');
        });

        // Bootstrap the application
        app.router = new app.AppRouter();
        Backbone.history.start();

    	// save init session time
    	this.utils.Storage.setSessionItem('init-session',new Date());
        this.utils.Storage.setLocalItem('loginModeGuest', false);
        this.utils.Storage.setLocalItem('skip_signin', false);
        this.utils.Storage.setLocalItem('logged-is-active', false);
        this.utils.Storage.setLocalItem('login-from-update', false);

        // init loader
        app.utils.Loader.initialize();

    },

    // Bind Event Listeners
    //
    bindEvents: function() {

    	document.addEventListener('deviceready', this.onDeviceReady, false);
    	document.addEventListener("backbutton", this.onBackKeyDown, false);
        // document.addEventListener("resume", this.onResume, false);

        document.addEventListener("mousemove", app.resetTimer, false);
        document.addEventListener("mousedown", app.resetTimer, false);
        document.addEventListener("keypress", app.resetTimer, false);
        document.addEventListener("DOMMouseScroll", app.resetTimer, false);
        document.addEventListener("mousewheel", app.resetTimer, false);
        document.addEventListener("touchmove", app.resetTimer, false);
        document.addEventListener("MSPointerMove", app.resetTimer, false);

        app.startTimer();
    },

    startTimer: function() {
        app.timeoutID = window.setTimeout(app.goInactive, (1000 * 60 * app.sessionPasswordTime));
    },

    resetTimer: function(e) {
        window.clearTimeout(app.timeoutID);
        if (app.utils.Storage.getLocalItem('logged-is-active') == true && app.utils.Storage.getLocalItem('skip_signin') == false) {
            app.startTimer();
        }
    },

    goInactive: function() {
        var currentPage = app.router.navigation[app.router.navigation.length-1];
        if (currentPage == 'chat') {
            app.timeoutID = window.setTimeout(app.goInactive, (1000 * 60));
        } else {
            if (app.utils.Storage.getLocalItem('logged-is-active') == true) {
                app.utils.Storage.setLocalItem('logged-is-active', false);
                if (app.utils.Storage.getLocalItem('remember') == true
                    || app.utils.Storage.getLocalItem('logged-guest') == true) {
                    document.location.href = 'index.html';
                } else {
                    showAlert("Sesión Expirada", "Estimado cliente su sesión ha expirado.", "OK", function () {
                        navigator.splashscreen.show();
                        window.setTimeout(function() {
                            document.location.href = 'index.html';
                        }, 1000);
                    });
                }
            }
        }
    },

    // deviceready Event Handler
    onDeviceReady: function() {

    	// init variable analytics
        window.analytics = window.ga;

        //Device informations
        window.build('', function(build){
            app.build = build;
            app.initializeGoogleAnalytics();
        });

        window.version('', function(version){

            app.version = version;

            // check if app version is enabled
            if(app.enableCheckAppVersion){
               app.checkAppVersion(app.version);
            }

            // initialize google analytics plugin
            app.initializeGoogleAnalytics();
        });

        // Init google analytics
    	app.initializeGoogleAnalytics();

    	// show app rate message
    	if(app.enableAppRate  && !app.utils.Storage.getLocalItem('outdated-app')){
    		app.showAppRate();
    	}

    	app.initializePushNotification();

    	// set uuid
    	if(app.uuid == null){
    	    app.uuid = device.uuid;
    	}
    },

    initializePushNotification: function(){

    	console.log('#PushNotification initialize...');
        app.pushNotification = window.plugins.pushNotification;
        app.pushNotification.register(
        	//success handler
        	function(result) {
        		 console.log('result = '+result);
        	},
        	//error handler
        	function(error) {
        		console.log('error');
        		console.log(error);
        	},
        	{
        		'senderID': app.senderId,
        		'ecb': 'app.onNotificationGCM'
        	}
        );
    },

    onNotificationGCM: function(e) {

    	console.log('#onNotification');

        switch(e.event){
            case 'registered':
            if (e.regid.length > 0) {
                // save registration ID
                app.registrationId = e.regid;
                console.log('GCM token '+app.registrationId);
            }
            break;

            case 'message':
                // if this flag is set, this notification happened while we were in the foreground.
                // you might want to play a sound to get the user's attention, throw up a dialog, etc.
            	console.log('Could start notification');

            	navigator.notification.alert(
            			e.message,							// message
                	    function(){},        				// callback
                	    'Mi Claro PR',         				// title
                	    'Ok'                  			    // buttonName
                	);
            break;

            case 'error':
                console.log('#error MSG:'+e.msg);
            break;

            default:
            	console.log('#error Unknown, an event was received and we do not know what');
            break;
        }
    },

    showAppRate: function() {

    	app.rate = AppRate;

    	// set properties
    	app.rate.preferences.storeAppURL.android = 'market://details?id='+app.packageName;
    	app.rate.preferences.useLanguage = 'es';
    	app.rate.preferences.displayAppName = 'Mi Claro';

        // open app rate prompt
        if (app.utils.Storage.getLocalItem('app-rated') == null || !app.utils.Storage.getLocalItem('app-rated')) {
        	app.rate.promptForRating(false);
        }
    },

    checkAppVersion: function(version) {
    	var method = 'app/getVersion',
    		parameters,
    		outdatedApp = app.utils.Storage.getLocalItem('outdated-app');

		    // if device hasn't connection
		    if(navigator.connection.type == Connection.NONE || navigator.connection.type == Connection.UNKNOWN) {

                navigator.splashscreen.hide();

                app.utils.Storage.setLocalItem('connection-inactive', true);
                app.router.navigate('login',{trigger: true});

                showConfirm(
                    'Error',
                    'Por favor compruebe su conexión a internet.',
                    ['Volver a Intentar', 'Salir'],
                    function (i) {
                        switch (i) {
                            case 1:
                                navigator.splashscreen.show();
                                location.reload();
                                break;
                            case 2:
                                navigator.app.exitApp();
                                break;
                        }
                    });

		    } else {

                app.utils.Storage.setLocalItem('connection-inactive', false);

                var goTo = '';
                const isLogged = app.utils.Storage.getLocalItem('isLogged');
                const isGuest = app.utils.Storage.getLocalItem('logged-guest');
                if (isLogged && isGuest) {
                    goTo = 'login_guest';
                } else {
                    goTo = 'login';
                }

                /**** TO SKIP CHECK VERSION ****/
                // navigator.splashscreen.hide();
                // app.utils.Storage.setLocalItem('outdated-app', false);
                // app.router.navigate(goTo, {trigger: true});
                // return;

		        parameters = JSON.stringify({
                    appId: app.id,
                    version: version,
                    osType: app.os
                });

	    		app.utils.network.checkRequest(method, parameters,
					//success function
					function(response) {
						navigator.splashscreen.hide();
		    			if(!response.hasError){
		    				if(response.object.enabled == 'Y'){
		    					app.utils.Storage.setLocalItem('outdated-app', false);
                                app.router.navigate(goTo, {trigger: true});
		    				} else {
                                app.utils.Storage.setLocalItem('outdated-app', true);
                                app.router.navigate('update_app',{trigger: true});
		    				}
		    			} else {
		    				app.utils.Storage.setLocalItem('outdated-app', true);
                            app.router.navigate('update_app',{trigger: true});
		    			}
					},
					// error function
					function(error) {
	                    navigator.splashscreen.hide();
	    		    	if(outdatedApp != null && outdatedApp) {
                            app.router.navigate('update_app', {trigger: true});
	    		    	} else {
	    		    		app.router.navigate(goTo, {trigger: true});
	    		    	}
					}
				);
		    }
    },

    handleOpenURL: function(url) {
        console.log('url: '+url);
        app.utils.Storage.setSessionItem('navegation-path', url);
        $.mobile.activePage.trigger('active');
    },

    removeSession: function() {
        app.utils.Storage.removeLocalItem('username');
        app.utils.Storage.removeLocalItem('password');
        app.utils.Storage.removeLocalItem('remember');

        app.utils.Storage.removeLocalItem('isLogged');
        app.utils.Storage.removeLocalItem('logged-guest');
        app.utils.Storage.removeLocalItem('logged-subscriber');
        app.utils.Storage.removeLocalItem('logged-subscriber-used');
        app.utils.Storage.setLocalItem('loginModeGuest', false);
        app.utils.Storage.setLocalItem('logged-is-active', false);
        app.utils.Storage.setLocalItem('login-from-update', false);

        app.utils.Storage.clearSessionStorage();
    },

    // Resume Event Handler for retrieved from the background.
    onResume: function(e) {
        var time = window.sessionStorage.getItem('time'),
        now = new Date(),
        expire = new Date();

        // set expire time
        expire.setTime(time);

        if(now.getTime() > time){
            // showAlert('Sesión Expirada', 'Su sesión ha expirado por inactivadad.', 'OK', function () {
            //     document.location.href='#login';
            // });
            document.location.href='#login';
        }
    },

    initializeGoogleAnalytics: function(){

        var states = {};

        states[Connection.UNKNOWN]  = 'Unknow connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI]     = 'WiFi connection';
        states[Connection.CELL_2G]  = '2G connection';
        states[Connection.CELL_3G]  = '3G connection';
        states[Connection.CELL_4G]  = '4G connection';
        states[Connection.NONE]     = 'None';

        if(app.version!=null && app.build!=null){

            analytics.startTrackerWithId(
                app.gaAccountID,
                function(success){
                    console.log('success google analitycs')
                },
                function(error){
                    console.log('error google analitycs')
                }
            );

            // send google statistics
            analytics.trackEvent('init', 'connection', 'connection type', states[navigator.connection.type]);

        }

    },

    onBackKeyDown: function(e) {
        console.log('back click on Android Interface');
        if (app.isMenuOpen == true) {
            var sideNav = document.getElementById("mySidenav");
            if (sideNav) {
                sideNav.style.right = "100%";
                $('#nav-close').hide();
                $('#nav-open').show();
                app.isMenuOpen = false;
            }
            return;
        }
	    // Handle the back button
    	app.router.backPage();
	}
};

//Extend underscore's template() to allow inclusions
function cTemplate(str) {

	// match "<% include template-id %>"
	var tempStr = str.replace(
        /<%\s*include\s*(.*?)\s*%>/g,
        function(match, templateId) {
        	var el = app.TemplateManager.templates[templateId];
        	if(el){
        		return el.html();
        	}else{
        		return '';
        	}
        });

    return _.template(tempStr);
}
