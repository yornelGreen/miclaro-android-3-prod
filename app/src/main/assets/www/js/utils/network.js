$(function() {

	// Utils For make AJAX Request
	// ---------------
	
	app.utils.Network = {
  
	    attempts: 0,

		errorMsg: 'Disculpe, no fue posible establecer la comunicación',

        errorRequest: function (data, status, message) {
	        if (message) {
                showAlert('Error', message, 'Aceptar', function(){});
            } else if (status == 401) {
                app.goInactive();
            } else {
                showAlert('Error', app.utils.network.errorMsg, 'Aceptar', function(){});
            }
        },

        processRequest: function(parameters, successCB, errorCB, skip) {

            var self = this;
            // Timeout time
            var timeoutValue = 90000;
            // Show loading
            app.utils.loader.show();

            if (self.attempts > 2) {
                return;
            }

            if (!skip && !app.utils.tools.isSessionActive()) {
                console.log('Session is expired!!!');
                app.utils.loader.hide();
                return;
            }

            // if device hasn't connection
            if (navigator.connection.type == Connection.NONE
                || navigator.connection.type == Connection.UNKNOWN) {
                app.utils.loader.hide();
                showConfirm(
                    'Error',
                    'Por favor compruebe su conexión a internet.',
                    ['Volver a Intentar', 'Salir'],
                    function (i) {
                        switch (i) {
                            case 1:
                                self.processRequest(parameters, successCB, errorCB, skip);
                                break;
                            case 2:
                                navigator.app.exitApp();
                                break;
                        }
                    });
                return;
            }

            if (!skip && !app.utils.tools.isSessionActive()) {
                console.log('Session is expired!!!');
                app.utils.loader.hide();
                return;
            }

            $.ajax({
                type: 'POST',
                url: app.processURL,
                contentType: 'application/json; charset=utf-8',
                data: parameters,
                dataType: 'json',
                timeout: timeoutValue,
                success: function (data, textStatus) {
                    app.utils.loader.hide();
                    successCB(data);
                },
                error: function (data, status, error) {
                    app.utils.loader.hide();
                    errorCB(data, status);
                }
            });
        },

        processRequestNotDialog: function(parameters, successCB, errorCB) {
            // Timeout time
            var timeoutValue = 90000;

            $.ajax({
                type: 'POST',
                url: app.processURL,
                contentType: 'application/json; charset=utf-8',
                data: parameters,
                dataType: 'json',
                timeout: timeoutValue,
                success: function (data, textStatus) {
                    successCB(data);
                },
                error: function (data, status, error) {
                    errorCB(data, status);
                }
            });
        },

        requestLogin: function(method, parameters, successCB, errorCB) {
            this.requestNew('login', method, null, parameters, successCB, errorCB)
        },

        requestCustomers: function(method, headers, parameters, successCB, errorCB) {
		    this.requestNew('customers', method, headers,  parameters, successCB, errorCB)
        },

        requestNew: function(path, method, headers, parameters, successCB, errorCB) {

            var self = this;
            // Timeout time
            var timeoutValue = 70000;
            // Show loading
            app.utils.loader.show();

            if (self.attempts > 2) {
                return;
            }

            if (navigator.connection.type == Connection.NONE
                || navigator.connection.type == Connection.UNKNOWN) {
                app.utils.loader.hide();
                //errorCB({}, 503, 'Por favor compruebe su conexión a internet.');
                showConfirm(
                    'Error',
                    'Por favor compruebe su conexión a internet.',
                    ['Volver a Intentar', 'Salir'],
                    function (i) {
                        switch (i) {
                            case 1:
                                self.requestNew(path, method, headers, parameters, successCB, errorCB);
                                break;
                            case 2:
                                navigator.app.exitApp();
                                break;
                        }
                    });
                return;
            }

            $.ajax({
                type: 'POST',
                url: app.newUrl + path + '/' + method,
                contentType: 'application/json; charset=utf-8',
                data: parameters,
                dataType: 'json',
                timeout: timeoutValue,
                headers: headers != null? headers : '',
                success: function (data, textStatus) {
                    app.utils.loader.hide();
                    successCB(data);
                },
                error: function (data, status, error) {
                    app.utils.loader.hide();
                    errorCB(data, status);
                }
            });
        },

		// Only for get stores
 		apiRequest: function(method, parameters, successCB, errorCB, loader){

			// Timeout time
			var timeoutValue = 50000;

			var  header = {
                            'api-key': app.apiKey
                          };

			// Show loading
            if(loader==undefined || loader) {
			    app.utils.loader.show();
            }
		    
			$.ajax({
				type: 'POST',
				cache: false,
                crossDomain: true,
			    url: app.apiUrl + method,  
			    contentType: 'application/json; charset=utf-8',
			    data: parameters,
			    dataType: 'json',
			    timeout: timeoutValue,
			    headers: header,
			    success: function (data, textStatus) { 
			        console.log('#ws call success');
                    		
			        // Hidden loading
                    if(loader==undefined || loader) {
				        app.utils.loader.hide();
                    }
			        successCB(data);
			    },
			    error: function (data, status, error) {
			    	console.log('#ws call error');

                    // Hidden loading
                    if(loader==undefined || loader) {
				    	app.utils.loader.hide();
                    }
			    	errorCB(data);
			    }
			});

		},

 		checkRequest: function(method, parameters, successCB, errorCB){

			// Timeout time
			var timeoutValue = 10000;

		    var  header = {
                            'api-key': app.apiKey
                          };

			$.ajax({
				type: 'POST',
			    url: app.apiUrl + method,
			    contentType: 'application/json; charset=utf-8',
			    data: parameters,
			    dataType: 'json',
			    timeout: timeoutValue,
			    headers: header,
			    success: function (data, textStatus) {
			        console.log('#ws call success');

			        // Hidden loading
			        app.utils.loader.hide();
			        successCB(data);
			    },
			    error: function (data, status, error) {
			    	console.log('#ws call error');
			    	errorCB(data);
			    }
			});
		},

        helpRequest: function(parameters, url, successCB, errorCB) {
            var self = this;
            // Timeout time
            var timeoutValue = 90000;
            // Show loading
            app.utils.loader.show();

            if (self.attempts > 2) {
                return;
            }

            $.ajax({
                type: 'POST',
                url: url,
                contentType: 'application/json; charset=utf-8',
                data: parameters,
                dataType: 'json',
                timeout: timeoutValue,
                success: function (data, textStatus) {
                    app.utils.loader.hide();
                    successCB(data);
                },
                error: function (data, status, error) {
                    app.utils.loader.hide();
                    errorCB(data);
                }
            });
        }
	};
});
