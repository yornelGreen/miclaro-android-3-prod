$(function () {

    // Offer Model
    // ----------

    app.models.Offer = Backbone.Model.extend({

        initialize: function () {

        },

        getPlans: function(creditClass, customerSubType, customerType, price, soc, technology, successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'getPlanes2';

            const parameters = JSON.stringify({
                creditClass: creditClass,
                customerSubType: customerSubType,
                customerType: customerType,
                price: price,
                soc: soc,
                tecnology: technology,
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        getPlansPrepaid: function(account ,subscriber, customerType, technology, successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'getChangePlanPrepago';

            const parameters = JSON.stringify({
                account: account,
                suscriber: subscriber,
                accountType: customerType,
                tech: technology,
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        getPlansDSL: function(subscriber, successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'DSLCatalog';

            const parameters = JSON.stringify({
                phoneNumber: subscriber,
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        updateSubscriberPlan: function(newSoc, oldSoc, productType, subscriber, successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'updateSubscriberPricePlanSocs';

            const parameters = JSON.stringify({
                NewSocCode: newSoc,
                OldSocCode: oldSoc,
                mProductType: productType,
                mSubscriberNo: subscriber,
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        updateSubscriberPlanNextCycle: function(newSoc, oldSoc, productType, subscriber, account, successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'updateSubscriberPricePlanSocsNextCicle';

            const parameters = JSON.stringify({
                BAN: account,
                NewSocCode: newSoc,
                OldSocCode: oldSoc,
                mProductType: productType,
                mSubscriberNo: subscriber,
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        updateSubscriberDSLPlan: function (productId, oldSocPrice, contract, alphaCodeContract, productType, subscriber, account, successCB, errorCB) {

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'adaDslPackageChange';

            const parameters = JSON.stringify({
                ProductType: productType,
                alphaCodeContract: alphaCodeContract,
                contract: contract,
                dslBan: account,
                dslPhoneNumber: subscriber,
                oldSocPrice: oldSocPrice,
                productId: productId,
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        updatePrepaidSubscriberPlan: function(amount, planName, rechargeMinutes, newSoc, currentSoc, accountType, accountSubType, account, subscriber, successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'changePrepaidAccount';

            const parameters = JSON.stringify({
                suscriber: subscriber,
                amount: amount,
                ban: account,
                currentAccountSubType: accountSubType,
                currentAccountType: accountType,
                currentSoc: currentSoc,
                newAccountSubType: accountSubType,
                newAccountType: accountType,
                newSoc: newSoc,
                planName: planName,
                rechargeMinutes: rechargeMinutes,
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        getDataPackets: function(groupID, transactionId, subscriber, offerID, successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'GetOffersToSubscriber';

            const parameters = {
                OfferGroup: '',
                SubscriberId: subscriber,
                TransactionId: transactionId,
                method: method,
                token: tokenSession
            };

            if (offerID) {
                parameters.offerID = offerID;
            }

            app.utils.network.processRequest(JSON.stringify(parameters), successCB, errorCB);
        },

        getReadSubscriber: function(subscriber, transactionId, successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'GetReadSubscriber';

            const parameters = JSON.stringify({
                IdSubscriber: subscriber,
                TransactionId: transactionId,
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        validateCreditLimit: function (account, accountTotalRent, productPrice, successCB, errorCB) {

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'ValidateCreditLimit';

            const parameters = JSON.stringify({
                Ban: btoa(account),
                ProductPrice: btoa(productPrice),
                AccountTotalRent: btoa(accountTotalRent),
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        addOfferToSubscriber: function (data, successCB, errorCB) {

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'AddOffersToSubscriber';

            data.method = method;
            data.token = tokenSession;

            const parameters = JSON.stringify(data);

            app.utils.network.processRequest(parameters, successCB, errorCB);
        }
    });

});