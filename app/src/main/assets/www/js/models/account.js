$(function() {

	// Application Model
	// ----------
	
	app.models.Account = Backbone.Model.extend({				
		
		initialize: function() {							
			
	    },

        addAccount: function (accountNumber, ssn, defaultAccount, successCB, errorCB) {

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'addAccounts';

            const parameters = JSON.stringify({
                account: accountNumber,
                ssn: ssn,
                setAsDefaultAccount: defaultAccount,
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        setDefaultAccount: function (accountNumber, subscriber, accountType, accountSubType, productType, successCB, errorCB) {

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'SetAsDefaultAccount';

            const parameters = JSON.stringify({
                account: accountNumber,
                subscriber: subscriber,
                accountType: accountType,
                accountSubType: accountSubType,
                productType: productType,
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        getAccounts: function(successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');

            const method = 'getAcounts';

            const parameters = JSON.stringify({
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        deleteAccount: function (accountNumber, successCB, errorCB) {

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'deleteAccount2';

            const parameters = JSON.stringify({
                account: accountNumber,
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        getHistoryOrders: function (account, successCB, errorCB) {

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'accountPackagesInfo';

            const parameters = JSON.stringify({
                Ban: account,
                method: method,
                token: tokenSession
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        subscribeNetflix: function(account, subscriber, successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'suscribirNeflix';

            const parameters = JSON.stringify({
                account: account,
                subscriber: subscriber,
                customerType: "",
                mdeviceSerialNumber: "",
                moperatorUrlError: "",
                mpromotionId: "",
                msalesChannel: "",
                productId: "",
                subProductId: "",
                method: method,
                token: tokenSession,
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        },

        resendCode: function(successCB, errorCB){

            const tokenSession = app.utils.Storage.getSessionItem('token');
            const method = 'resendCode';

            const parameters = JSON.stringify({
                method: method,
                token: tokenSession,
            });

            app.utils.network.processRequest(parameters, successCB, errorCB);
        }
    });
	
});