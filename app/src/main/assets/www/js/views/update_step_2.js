$(function() {

    // Register step 1 View
    // ---------------

    app.views.UpdateStep2View = app.views.CommonView.extend({

        name: 'update_step_2',

        // The DOM events specific.
        events: {
            // event
            'pagecreate':                           	'pageCreate',

            // content
            'click #btn-validate':                      'validateNext',
            'click #btn-login':                         'login',
            'input #ssn':                               'ssnChanged',
            'input #code':                              'codeChanged',
            'click #btn-resend':                        'send',

            // footer
            'click #btn-help':		                    'helpSection',
        },

        // Render the template elements
        render: function(callback) {
            var self = this,
                variables = {
                    isTelephony: app.utils.Storage.getSessionItem('update-user-is-telephony'),
                    isProductTypeG: app.utils.Storage.getSessionItem('update-user-by-code'),
                    showBackBth: true
                };
            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                callback();
                return this;
            });
        },

        pageCreate: function(){
            var self = this;
            // removing any enter event
            $('body').unbind('keypress');

            /**
             * set enter event
             */
            $('body').on('keypress', function(e){
                if (e.which === 13 || e.keyCode === 13) {
                    self.validateNumber();
                }
            });

            // enable tooltips
            $('[data-toggle="popover"]').popover({
                animation: false
            });

            $.mobile.activePage.on("show.bs.popover", ".tooltip-pin", function(event) {
                setTimeout(function(){
                    $.mobile.activePage.find('[data-toggle="popover"]').popover('hide');
                },3000);
            });
        },

        ssnChanged: function(e) {
            self = this;

            var number = $.mobile.activePage.find('#ssn').val();

            if (number.length > 4) {
                number = number.slice(0,4);
                $.mobile.activePage.find('#ssn').val(number);
            }
            self.availableNext(e);
        },

        codeChanged: function(e) {
            var self = this;

            var number = $.mobile.activePage.find('#code').val();

            if (number.length > 6) {
                number = number.slice(0,6);
                $.mobile.activePage.find('#code').val(number);
            }
            self.availableNext(e);
        },

        login: function(e) {
            //Go to next
            app.router.navigate('login', {
                trigger: true
            });

        },

        validateNext: function(e) {
            self = this;

            var number = app.utils.Storage.getSessionItem('update-user-subscriber');

            var code ='';
            var ssn = '';


            var isProductTypeG =  app.utils.Storage.getSessionItem('update-user-by-code');

            if (isProductTypeG) {
                code = $.mobile.activePage.find('#code').val();
                if (code.length !== 6) {
                    message = 'Debe ingresar los datos solicitados.';
                    showAlert('Error', message, 'Aceptar');
                    return;
                }

            } else {
                ssn = $.mobile.activePage.find('#ssn').val();
                if (ssn.length != 4) {
                    message = 'Debe ingresar los datos solicitados.';
                    showAlert('Error', message, 'Aceptar');
                    return;
                }
            }

            /**
             * Update User
             */
            $('#code').blur();
            $('#ssn').blur();
            self.options.customerModel.updateValidateAccount(number, code, ssn,
                function (response) {
                    if(response.hasError){
                        showAlert('Error', response.errorDisplay, 'Aceptar');
                    } else {
                        app.utils.Storage.setLocalItem('isLogged', true);
                        app.utils.Storage.setLocalItem('logged-is-active', true);
                        app.utils.Storage.setLocalItem('login-from-update', true);
                        app.router.navigate('login', {
                            trigger: true
                        });
                    }
                },
                app.utils.network.errorRequest
            );
        },

        availableNext: function(e) {

            var isProductTypeG =  app.utils.Storage.getSessionItem('update-user-by-code');
            var code = $.mobile.activePage.find('#code').val();
            var ssn = $.mobile.activePage.find('#ssn').val();

            if ((isProductTypeG && code.length === 6) || (!isProductTypeG && ssn.length === 4)) {
                $('#btn-validate').removeClass('gray').addClass('red').addClass('rippleR');
            } else {
                $('#btn-validate').removeClass('red').removeClass('rippleR').addClass('gray');
            }
        },

        send: function(e) {
            self = this;

            const number = app.utils.Storage.getSessionItem('update-user-subscriber');
            self.options.customerModel.validateSubscriberUpdate(number,
                function (response) {
                    if(response.hasError){
                        showAlert('Error', response.errorDisplay, 'Aceptar');
                    } else {
                        showAlert('', 'Código de seguridad enviado.','ok');
                    }
                },
                app.utils.network.errorRequest
            );
        }

    });

});
