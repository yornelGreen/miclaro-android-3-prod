$(function() {

    app.views.ClubView = app.views.CommonView.extend({

        name: 'club',

        // The DOM events specific.
        events: {
            // events
            'pagecreate':                           'pageCreate',

            // content
            'click #subscription':					'subscription',
            'click #show-terms':                    'showTerms',
            'click #faq':					        'faq',
            'click #support':					    'support',
            'click #recover-email':					'recoverEmail',
            'click #recover-password':			    'recoverPassword'
        },

        // Render the template elements
        render: function(callback) {

            if (app.utils.Storage.getSessionItem('token') == null) {
                document.location.href = 'index.html';
                return;
            }

            var self = this,
                variables = {
                    accounts: this.getSelectTabAccounts(),
                    selectedTab: app.utils.Storage.getSessionItem('selected-tab'),
                    selectedAccount: app.utils.Storage.getSessionItem('selected-account'),
                    accountSections: this.getUserAccess(),
                    showBackBth: true
                };
            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                callback();
                return this;
            });
            $(document).scrollTop();
        },

        pageCreate: function(e) {
            var self = this;
            self.activateMenu(e);
        },

        subscription: function(e) {
            if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
                window.location.href = 'https://play.google.com/store/apps/details?id=com.mlstudio.burea';
            }
            if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){
                window.location.href = 'https://apps.apple.com/us/app/burea/id978694147';
            }
        },

        showTerms: function(e) {
            app.router.navigate('club_terms', {trigger: true});
        },

        faq: function (e) {
            app.router.navigate('club_faq', {trigger: true});
        },
    });
});
