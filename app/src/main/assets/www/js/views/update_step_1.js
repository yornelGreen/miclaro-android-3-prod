$(function() {

    // Register step 1 View
    // ---------------

    app.views.UpdateStep1View = app.views.CommonView.extend({

        name: 'update_step_1',

        // The DOM events specific.
        events: {
            // event
            'pagecreate':                           	'pageCreate',

            // content
            'click #btn-validate':                      'validateNumber',
            'click #btn-login':                         'login',
            'input #register_number':                   'numberChanged',

            // footer
            'click #btn-help':		                    'helpSection',
        },

        // Render the template elements
        render: function(callback) {
            var self = this,
                variables = {
                    showBackBth: true
                };
            app.TemplateManager.get(self.name, function(code) {
                var template = cTemplate(code.html());
                $(self.el).html(template(variables));
                callback();
                return this;
            });
        },

        pageCreate: function(){
            var self = this;
            // removing any enter event
            $('body').unbind('keypress');

            /**
             * set enter event
             */
            $('body').on('keypress', function(e){
                if (e.which === 13 || e.keyCode === 13) {
                    self.validateNumber();
                }
            });

            $('#register_number').on('click focus', function (e) {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#register_number").offset().top-20
                }, 1000);
            });
        },

        numberChanged: function() {
            var number = $.mobile.activePage.find('#register_number').val();

            if (number.length > 10) {
                number = number.slice(0,10);
                $.mobile.activePage.find('#register_number').val(number);
            }
        },

        login: function(e) {
            //Go to next
            app.router.navigate('login', {
                trigger: true
            });

        },

        validateNumber: function(e) {
            self = this;

            var number = $.mobile.activePage.find('#register_number').val();

            // validate
            if (number == null || !number.length > 0) {
                message = 'Debe ingresar un número de suscriptor válido.';
                showAlert('Error', message, 'Aceptar');
                return;
            } else if (number.length < 10) {
                message = 'Debe ingresar un número de suscriptor válido.';
                showAlert('Error', message, 'Aceptar');
                return;
            }

            /**
             * Validate User
             */
            $('#register_number').blur();
            self.options.customerModel.validateSubscriberUpdate(number,
                function (response) {
                    if(response.hasError){
                        showAlert('Error', response.errorDisplay, 'Aceptar');
                    } else {
                        app.utils.Storage.setSessionItem('update-user-subscriber', number);
                        app.utils.Storage.setSessionItem('update-user-by-code', response.productType === 'G');
                        app.utils.Storage.setSessionItem('update-user-is-telephony',
                            app.utils.tools.accountIsTelephony(response.accountType, response.accountSubType, response.productType));
                        app.router.navigate('update_step_2', {
                            trigger: true
                        });
                    }
                },
                app.utils.network.errorRequest
            );
        }

    });

});
