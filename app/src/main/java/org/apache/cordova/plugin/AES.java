package org.apache.cordova.plugin;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyException;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.util.Base64;

/**
 * This class echoes a string called from JavaScript.
 */
public class AES extends CordovaPlugin {
	
    private final static String characterEncoding = "UTF-8";
	private final static String cipherTransformation = "AES/CBC/PKCS5Padding";
	private final static String aesEncryptionAlgorithm = "AES";
	
	private static final String ENCRYPTION_KEY = "qcPQK9012G3G7DCt";
	private static final String ENCRYPTION_IV = "4W4NtvbLf85vUTZ3";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("encryp")) {
            String plainText = args.getString(0); 
            this.encrypAES(plainText, callbackContext);
            return true;
        }
        return false;
    }

    private void encrypAES(String plainText, CallbackContext callbackContext) {
    	
        if (plainText != null && plainText.length() > 0) { 
            try {
				callbackContext.success(AES.encrypt(plainText, ENCRYPTION_KEY,ENCRYPTION_IV));			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				callbackContext.error("Expected one non-empty string argument.");
			}
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
    	
	public static String encrypt(String plainText, String key, String ivy) throws KeyException, GeneralSecurityException, GeneralSecurityException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException{
		Cipher cipher = Cipher.getInstance(cipherTransformation);
        SecretKeySpec secretKeySpec = new SecretKeySpec(getKeyBytes(key), aesEncryptionAlgorithm);
		IvParameterSpec ivParameterSpec = new IvParameterSpec(getKeyBytes(ivy));
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
		byte[] plainTextBytes = cipher.doFinal(plainText.getBytes());
		return new String(Base64.encode(plainTextBytes,Base64.DEFAULT));
	}
	
	public static String decrypt(String plainText, String key, String ivy) throws KeyException, GeneralSecurityException, GeneralSecurityException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException{
		Cipher cipher = Cipher.getInstance(cipherTransformation);
		SecretKeySpec secretKeySpec = new SecretKeySpec(getKeyBytes(key), aesEncryptionAlgorithm);
		IvParameterSpec ivParameterSpec = new IvParameterSpec(getKeyBytes(ivy));
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
		byte[] plainTextBytes = cipher.doFinal(plainText.getBytes());
		return new String(Base64.encode(plainTextBytes,Base64.DEFAULT));
	}

    private static byte[] getKeyBytes(String key) throws UnsupportedEncodingException{
        byte[] keyBytes= new byte[16];
        byte[] parameterKeyBytes= key.getBytes(characterEncoding);
        System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
        return keyBytes;
    }
	
}