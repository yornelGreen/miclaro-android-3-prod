/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.todoclaro.miclaroapp;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import org.apache.cordova.*;

public class MainActivity extends CordovaActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        // enable Cordova apps to be started in the background
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getBoolean("cdvStartInBackground", false)) {
            moveTaskToBack(true);
        }

        // Set by <content src="index.html" /> in config.xml
        loadUrl(launchUrl);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        try {
            processUri(intent.getData());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void processUri (Uri uri) throws InterruptedException{

        if(uri != null){

            String action = uri.getHost();
            if(action != null){
                //Log.d("MainActivity.processUri", " *********** processUri URI action is: " + action );
                loadUrl("javascript:app.handleOpenURL('" + action + "');");
            }else{
                //Log.d("MainActivity.processUri", " *********** processUri URI button is NULL    *********** ");
            }
        }else{
            //Log.d("MainActivity.processUri", " *********** processUri URI is NULL    *********** ");
        }
    }

    public class DownloadTask extends AsyncTask<String, Void, String> {

        public Uri uri;

        public DownloadTask(Uri uri) {
            // TODO Auto-generated constructor stub
            this.uri = uri;
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {
                Thread.currentThread();
                Thread.sleep(2000);



            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(uri != null){

                String action = uri.getHost();
                if(action != null){
                    //Log.d("MainActivity.processUri", " *********** onPostExecute URI action is: " + action );
                    loadUrl("javascript:app.handleOpenURL('" + action + "');");
                }else{
                    //Log.d("MainActivity.processUri", " *********** onPostExecute URI button is NULL    *********** ");
                }
            }else{
                //Log.d("MainActivity.processUri", " *********** onPostExecute URI is NULL    *********** ");
            }

        }

    }

}
