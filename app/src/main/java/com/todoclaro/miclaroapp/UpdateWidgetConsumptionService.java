package com.todoclaro.miclaroapp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

public class UpdateWidgetConsumptionService extends Service {


//	private String APP_SCHEME;
//	private String CUSTOM_ACTION;
	private String PRODUCT_TYPE;
	private String CLARO_WIDGET_SERVICE_URL;
	private Integer TOAST_DURATION;
	
	
	@Override
	public void onStart(Intent intent, int startId) {
		
		TOAST_DURATION 				= Integer.parseInt(getString(R.string.widget_toast_duration));
		

		SharedPreferences prefs 	= PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
		String loginHashCode 		= cleanStoredString(prefs.getString("login", ""));
		CLARO_WIDGET_SERVICE_URL	= cleanStoredString(prefs.getString("api-url", ""));
		PRODUCT_TYPE				= cleanStoredString(prefs.getString("productType", ""));
		

		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());
		int[] allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);

		for (int widgetId : allWidgetIds) {
			RemoteViews remoteViews = new RemoteViews(this.getApplicationContext().getPackageName(), R.layout.widget_consumption_layout);
			
			remoteViews.setInt(R.id.refreshButton, "setVisibility", View.INVISIBLE);
			
			// refresh button listener
			Intent clickIntent = new Intent(this.getApplicationContext(), WidgetConsumptionProvider.class);
			clickIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
			clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, clickIntent,PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.refreshButton, pendingIntent);
			
			if(loginHashCode == null || loginHashCode.isEmpty()
					|| CLARO_WIDGET_SERVICE_URL == null || CLARO_WIDGET_SERVICE_URL.isEmpty()	
					){
				
				remoteViews.setInt(R.id.refreshButton, "setVisibility", View.VISIBLE);
				
				remoteViews.setTextViewText(R.id.textViewMinutes, getString(R.string.widget_consumption_text_min_def));
				remoteViews.setTextViewText(R.id.textViewSMS, getString(R.string.widget_consumption_text_sms_def));
				
				remoteViews.setTextViewText(R.id.textViewDataTitle, getString(R.string.widget_consumption_text_no_data));
				remoteViews.setTextViewText(R.id.textViewDataPlan, "");
				remoteViews.setTextViewText(R.id.textViewDataPlanConsumption, "");
				
				remoteViews.setProgressBar(R.id.progressBar1, 100, 1, false);
				
				
				Toast.makeText(
						this.getApplicationContext(), 
						getString(R.string.widget_consumption_msg_no_logged_in),
						TOAST_DURATION).show();
			}else{
				
				new DownloadTask(
						this.getApplicationContext(),
						loginHashCode
						).execute();
			}
			
			
			appWidgetManager.updateAppWidget(widgetId, remoteViews);
		}
		
		stopSelf();

		super.onStart(intent, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	
	
	public class DownloadTask extends AsyncTask<String, Void, String> {

		public String appLogin;
		public StringBuilder text;
		public BufferedReader in;
		public URL url;
		
		private Context context;
		
		
		public DownloadTask(Context context, String appLogin){
			this.context = context;
			this.appLogin = appLogin;
		}
		
		@Override
		protected String doInBackground(String... arg0) {

			String request = "{\"token\":\"" + this.appLogin + "\"}";
			Log.i("sendServerData token:",  this.appLogin);
			
			StringBuffer response = null;
			HttpURLConnection myURLConnection = null;

	    	try {
	    		URL myURL = new URL(CLARO_WIDGET_SERVICE_URL);
				Log.i("sendServerData url:", myURL.toURI().toString());
	    		myURLConnection = (HttpURLConnection)myURL.openConnection();
	    		
	    		response = new StringBuffer();
		    	myURLConnection.setRequestMethod("GET");
		    	myURLConnection.setRequestProperty("Content-Type", "application/json");
		    	myURLConnection.setRequestProperty("Accept", "application/json");
				myURLConnection.setRequestProperty("api-key", "6af3982a-ce65-41a0-93d9-52bd172685cd");
		    	
		    	myURLConnection.setRequestProperty("Content-Length", "" + request.getBytes().length);
		    	myURLConnection.setUseCaches(false);
		    	myURLConnection.setDoInput(true);
		    	myURLConnection.setDoOutput(true);
		
		    	// Send request
		    	OutputStream os = myURLConnection.getOutputStream();
		    	BufferedWriter writer = new BufferedWriter(
		    	        new OutputStreamWriter(os, "UTF-8"));
		    	writer.write(request);
		    	writer.flush();
		    	writer.close();
		    	os.close();
		    	
		    	//Get Response	
		        InputStream is = myURLConnection.getInputStream();
		        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		        String line;
		         
		        while((line = rd.readLine()) != null) {
		          response.append(line);
		          response.append('\r');
		        }
		        rd.close();	        
		        
		        Log.i("sendServerData message:",  response.toString());
		        
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    	} finally {
	    		if(myURLConnection != null)
	    		myURLConnection.disconnect();
	    	}

			return (response != null) ? response.toString() : null;
		}

		@Override
		protected void onPostExecute(String result) {

			String minutes 			= "--";
			String sms 				= "--";
			String dataTitle 		= getString(R.string.widget_consumption_text_no_data);
			String plan 			= "--";
			String planConsumption 	= "--";

			Double used 	= 0D;
			Double total 	= 0D;
			
			RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.widget_consumption_layout);
			remoteViews.setInt(R.id.refreshButton, "setVisibility", View.VISIBLE);

			result = result.trim();
			result = result.replace("\n", "");
			result = result.replace("\t", "");
			result = result.replace("\r", "");

            if (result == null || result.isEmpty()) {
            	// could not get content, use cache
            	// could be null
	            Toast.makeText(this.context, getString(R.string.widget_msg_server_error), TOAST_DURATION).show();
	            
            }else{

            	try {
            		
            		JSONObject data = new JSONObject(result);
            		
            		if("false".equalsIgnoreCase(data.getString("hasError"))){
            			
            			if("true".equalsIgnoreCase(data.getString("response"))){
            				
            				JSONObject consumption 		= data.getJSONObject("object").getJSONObject("consumption");
            				String mobileProductType 	= "G";	// mobile product type

            				if(mobileProductType.equals(PRODUCT_TYPE)){
	            				JSONObject consumptionData 	= consumption.getJSONObject("data");
	            				
	            				minutes		= (consumption.has("voice")) ? consumption.getString("voice") : "0" ;
	            				sms 		= (consumption.has("sms")) ? consumption.getString("sms") : "0" ;
            				
            					dataTitle	= getString(R.string.widget_consumption_text_data);
            					plan 		= (consumptionData.has("plan")) ? consumptionData.getString("plan") : "--";
            					used 		= (consumptionData.has("used") && consumptionData.getDouble("used") >= 0) ? consumptionData.getDouble("used") : 0;
            					total 		= (consumptionData.has("total") && consumptionData.getDouble("total") >= 0) ? consumptionData.getDouble("total") : 0;
            					
            					String a 	= readableFileSize(used.doubleValue());
            					String b 	= readableFileSize(total.doubleValue());
            					
            					planConsumption = getString(R.string.widget_consumption_consumption);

            					if (plan.contains("PUJ") || plan.contains("Ilimitado")) {
									planConsumption = a + " usados";
								} else {
									planConsumption = (planConsumption.replace("XX", a)).replace("YY", b);
								}
            				
            				}
            				else{
            					
            					plan			= "";
            					planConsumption = "";
            				}

            			}else{
                			
                			Toast.makeText(this.context, getString(R.string.widget_msg_server_response_error), TOAST_DURATION).show();
                		}	
            		}else{
            			
            			Toast.makeText(this.context, getString(R.string.widget_msg_server_error), TOAST_DURATION).show();
            		}

    			} catch (JSONException e) {
    				e.printStackTrace();
    				Toast.makeText(this.context, getString(R.string.widget_msg_server_data_response_error), TOAST_DURATION).show();
    			}

            }

			AppWidgetManager widgetManager = AppWidgetManager.getInstance(getApplication());

			// get widget ids for widgets created
			ComponentName widget = new ComponentName(getApplication(), WidgetConsumptionProvider.class);
			int[] widgetIds = widgetManager.getAppWidgetIds(widget);
			
			
			// updating values
			remoteViews.setTextViewText(R.id.textViewMinutes, minutes);
			remoteViews.setTextViewText(R.id.textViewSMS, sms);
			remoteViews.setTextViewText(R.id.textViewDataTitle, dataTitle);
			remoteViews.setTextViewText(R.id.textViewDataPlan, plan);
			remoteViews.setTextViewText(R.id.textViewDataPlanConsumption, planConsumption);
			
			
			if(total.intValue() >= Integer.MAX_VALUE) {
				
				double divisor 		= total / (double)Integer.MAX_VALUE;
				
				Double tempUsed 	= used / divisor;
				Double tempTotal 	= total / divisor;
				
				remoteViews.setProgressBar(R.id.progressBar1, tempTotal.intValue(), tempUsed.intValue(), false);
				
			}else{
				
				remoteViews.setProgressBar(R.id.progressBar1, total.intValue(), used.intValue(), false);
			}
			
			
			// refreshing reload listener
			Intent clickIntent = new Intent(getApplicationContext(), WidgetConsumptionProvider.class);
			clickIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
			clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIds);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.refreshButton, pendingIntent);

			//refresh widget to show text
			widgetManager.updateAppWidget(widgetIds, remoteViews);
		}
	}
	
	
	public static String readableFileSize(Double bytes) {
	    
		if(bytes <= 0) return "0 MB";
		
	    final String[] units = new String[] { "B", "kB", "MB", "GB", "TB", };
	    int digitGroups = (int) (Math.log10(bytes)/Math.log10(1024));
	    
	    return new DecimalFormat("#,##0.##").format(bytes/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}
	
	
	public String cleanStoredString(String value){
		String cleaned = value.replace("\n", "");
		cleaned = value.trim();
		return cleaned;
	}
}