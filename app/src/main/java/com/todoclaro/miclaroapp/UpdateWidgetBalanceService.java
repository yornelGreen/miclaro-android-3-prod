package com.todoclaro.miclaroapp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

public class UpdateWidgetBalanceService extends Service {


	private String APP_SCHEME;
	private String CUSTOM_ACTION;
	private String CLARO_WIDGET_SERVICE_URL;
	private Integer TOAST_DURATION;
	
	private final String nextInvoiceDateKey = "nextInvoiceDate";
	private final String amountKey 			= "amount";
	private final String invoiceDateKey 	= "invoiceDate";
	
	
	@Override
	public void onStart(Intent intent, int startId) {
		
		Log.d("UpdateWidget onStart", "Empezando Service");


		APP_SCHEME 					= getString(R.string.app_scheme);
		CUSTOM_ACTION				= getString(R.string.custom_action);
		TOAST_DURATION 				= Integer.parseInt(getString(R.string.widget_toast_duration));
		

		SharedPreferences prefs 	= PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
		String loginHashCode 		= cleanStoredString(prefs.getString("login", ""));
		CLARO_WIDGET_SERVICE_URL	= cleanStoredString(prefs.getString("api-url", ""));
		

		
		
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());
		int[] allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);

		for (int widgetId : allWidgetIds) {
			RemoteViews remoteViews = new RemoteViews(this.getApplicationContext().getPackageName(), R.layout.widget_balance_layout);
			Log.d("UpdateWidget onStart", "definiendo listener");
			
			remoteViews.setInt(R.id.refreshButton, "setVisibility", View.INVISIBLE);
			
			
			// refresh button listener
			Intent clickIntent = new Intent(this.getApplicationContext(), WidgetBalanceProvider.class);
			clickIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
			clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, clickIntent,PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.refreshButton, pendingIntent);

			
			// footer buttons listeners
			// account
			Intent accountIntent = new Intent( CUSTOM_ACTION, Uri.parse(APP_SCHEME + "://account") );
			accountIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent accountPendingIntent = PendingIntent.getActivity
					(getApplicationContext(), 0, accountIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.footerButtonMC, accountPendingIntent);
			
			// invoice
			Intent invoiceIntent = new Intent(CUSTOM_ACTION, Uri.parse(APP_SCHEME + "://invoice") );
			invoiceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent invoicePendingIntent = PendingIntent.getActivity
					(getApplicationContext(), 0, invoiceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.footerButtonPF, invoicePendingIntent);
			
			// chat
			Intent chatIntent = new Intent(CUSTOM_ACTION, Uri.parse(APP_SCHEME + "://chat") );
			chatIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent chatPendingIntent = PendingIntent.getActivity
					(getApplicationContext(), 0, chatIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.footerButtonC, chatPendingIntent);			
			
			
			
			if(loginHashCode == null || loginHashCode.isEmpty()
					|| CLARO_WIDGET_SERVICE_URL == null || CLARO_WIDGET_SERVICE_URL.isEmpty()	
					){
				
				remoteViews.setInt(R.id.refreshButton, "setVisibility", View.VISIBLE);
				
				Toast.makeText(
						this.getApplicationContext(), 
						getString(R.string.widget_balance_msg_no_logged_in),
						TOAST_DURATION).show();
			}else{
				
				// Cache 

				/*** Caché commented **/
//				Date today 					= Calendar.getInstance().getTime();
//				Long nextInvoiceDateLong	= prefs.getLong(nextInvoiceDateKey, -1);
				
//				if(nextInvoiceDateLong == -1 || today.getTime() > nextInvoiceDateLong) {
					
					Log.d("UpdateWidget onStart", "a llamar a DownloadTask ");
					Log.d("UpdateWidget onStart", loginHashCode);
					new DownloadTask(
							this.getApplicationContext(),
							loginHashCode
							).execute();
					
//				}else{
//					
//					remoteViews.setInt(R.id.refreshButton, "setVisibility", View.VISIBLE);
//					remoteViews.setTextViewText(R.id.textViewBalanceAmount, prefs.getString(amountKey, getString(R.string.widget_balance_text_balance_def)));
//					remoteViews.setTextViewText(R.id.textViewInvoiceDate, prefs.getString(invoiceDateKey, getString(R.string.widget_balance_text_invoice_def)));
//					
//					Date nextInoviceDateDate 	= new Date(nextInvoiceDateLong);
//					SimpleDateFormat sdf 		= new SimpleDateFormat("MM/dd/yyyy");
//					
//					Log.d("UpdateWidgetBalanceService onStart", 
//							"No ha pasado la fecha de la facturación " + 
//									sdf.format(nextInoviceDateDate) + ", NO ES NECESARIO llamar al servicio");
//				}
			}
			
			appWidgetManager.updateAppWidget(widgetId, remoteViews);
		}
		
		stopSelf();

		super.onStart(intent, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	
	
	public class DownloadTask extends AsyncTask<String, Void, String> {

		public String appLogin;
		public StringBuilder text;
		public BufferedReader in;
		public URL url;
		
		private Context context;
		
		
		public DownloadTask(Context context, String appLogin){
			this.context = context;
			this.appLogin = appLogin;
		}
		
		@Override
		protected String doInBackground(String... arg0) {
			
			
			Log.d("UpdateWidget Task", "Empezando AsyncTask");
			String request = 
					"{\"token\":\"" 
					+ this.appLogin +
					"\"}";
			
			Log.d("UpdateWidget Task", "URL: " + CLARO_WIDGET_SERVICE_URL);
			Log.d("UpdateWidget Task", "Request: ");
			Log.d("UpdateWidget Task", request);
			
			
			StringBuffer response = null;
			HttpURLConnection myURLConnection = null;
			
	    	try {
	    		URL myURL = new URL(CLARO_WIDGET_SERVICE_URL);
	    		myURLConnection = (HttpURLConnection)myURL.openConnection();
	    		
	    		response = new StringBuffer();
		    	myURLConnection.setRequestMethod("POST");
		    	myURLConnection.setRequestProperty("Content-Type", "application/json");
		    	myURLConnection.setRequestProperty("Accept", "application/json");
				myURLConnection.setRequestProperty("api-key", "6af3982a-ce65-41a0-93d9-52bd172685cd");
		    	
		    	myURLConnection.setRequestProperty("Content-Length", "" + Integer.toString(request.toString().getBytes().length));
		    	myURLConnection.setUseCaches(false);
		    	myURLConnection.setDoInput(true);
		    	myURLConnection.setDoOutput(true);
		
		    	// Send request
		    	OutputStream os = myURLConnection.getOutputStream();
		    	BufferedWriter writer = new BufferedWriter(
		    	        new OutputStreamWriter(os, "UTF-8"));
		    	writer.write(request.toString());
		    	writer.flush();
		    	writer.close();
		    	os.close();
		    	
		    	//Get Response	
		        InputStream is = myURLConnection.getInputStream();
		        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		        String line;
		         
		        while((line = rd.readLine()) != null) {
		          response.append(line);
		          response.append('\r');
		        }
		        rd.close();	        
		        
		        Log.i("sendServerData message:",  response.toString());
		        
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    	} finally {
	    		if(myURLConnection != null)
	    		myURLConnection.disconnect();
	    	}
			
			
			Log.d("UpdateWidget Task", "Returning text.toString()");
			return (response != null) ? response.toString() : null;
		}

		
		
		@Override
		protected void onPostExecute(String result) {

			String balanceAmount 	= "--";
			String invoiceDate 		= "--";
			
			RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.widget_balance_layout);
			remoteViews.setInt(R.id.refreshButton, "setVisibility", View.VISIBLE);
			
			
			Log.d("UpdateWidget Execute", "result: " + result);
			
			result = result.trim();
			result = result.replace("\n", "");
			result = result.replace("\t", "");
			result = result.replace("\r", "");
            
            Log.d("UpdateWidget Execute", "result cleaned: " + result);
            
            if (result == null || result.isEmpty()) {
	            Log.d("UpdateWidget Execute", "result is null ");
	            Toast.makeText(this.context, getString(R.string.widget_msg_server_error), TOAST_DURATION).show();
	            
            }else{
            	
            	// we have content
            	
            	try {
            		
            		JSONObject data = new JSONObject(result);
            		
            		// is it valid?
            		if("false".equalsIgnoreCase(data.getString("hasError"))){
            			
            			if("true".equalsIgnoreCase(data.getString("response"))){
            		
            				Log.d("UpdateWidget Execute", "obteniendo los valores");
            				
            				JSONObject balance 		= data.getJSONObject("object").getJSONObject("balance");
            				
		    				balanceAmount	= "$" + balance.getString("amount");
		    				invoiceDate 	= balance.getString("invoiceExpirationDate");
		    				
		    				String invoiceDateComplete 	= balance.getString("invoiceExpirationDateComplete");
		    				SharedPreferences prefs 	= PreferenceManager.getDefaultSharedPreferences(context);
		    				SimpleDateFormat sdf 		= new SimpleDateFormat("MM/dd/yyyy");
		    				
		    				try {
		    					
								Date invoiceDateCompleteDate	= sdf.parse(invoiceDateComplete);
								Long nextInvoiceDateLong 		= prefs.getLong(nextInvoiceDateKey, -1);
								
								if(nextInvoiceDateLong == -1 || invoiceDateCompleteDate.getTime() > nextInvoiceDateLong) {
									
									Editor editor = prefs.edit();
									
									editor.putLong(nextInvoiceDateKey, invoiceDateCompleteDate.getTime());
									editor.putString(amountKey, balanceAmount);
									editor.putString(invoiceDateKey, invoiceDate);
									
									editor.commit();
								}
								
							} catch (ParseException e) {
								e.printStackTrace();
							}

		    				
            			}else{
                			
                			Toast.makeText(this.context, getString(R.string.widget_msg_server_response_error), TOAST_DURATION).show();
                		}	
            		}else{
            			
            			Toast.makeText(this.context, getString(R.string.widget_msg_server_error), TOAST_DURATION).show();
            		}
    			} catch (JSONException e) {
    				e.printStackTrace();
    				Toast.makeText(this.context, getString(R.string.widget_msg_server_data_response_error), TOAST_DURATION).show();
    			}

            }

            
            Log.d("UpdateWidget Execute", "refresh values and listeners");
			
			AppWidgetManager widgetManager = AppWidgetManager.getInstance(getApplication());

			// get widget ids for widgets created
			ComponentName widget = new ComponentName(getApplication(), WidgetBalanceProvider.class);
			int[] widgetIds = widgetManager.getAppWidgetIds(widget);
			
			
			// updating values
			remoteViews.setTextViewText(R.id.textViewBalanceAmount, balanceAmount);
			remoteViews.setTextViewText(R.id.textViewInvoiceDate, invoiceDate);
			
			// refreshing reload listener
			Intent clickIntent = new Intent(getApplicationContext(), WidgetBalanceProvider.class);
			clickIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
			clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIds);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.refreshButton, pendingIntent);
			
			
			// footer buttons listeners
			// account
			Intent accountIntent = new Intent( CUSTOM_ACTION, Uri.parse(APP_SCHEME + "://account") );
			accountIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent accountPendingIntent = PendingIntent.getActivity
					(getApplicationContext(), 0, accountIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.footerButtonMC, accountPendingIntent);
			
			// invoice
			Intent invoiceIntent = new Intent(CUSTOM_ACTION, Uri.parse(APP_SCHEME + "://invoice") );
			invoiceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent invoicePendingIntent = PendingIntent.getActivity
					(getApplicationContext(), 0, invoiceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.footerButtonPF, invoicePendingIntent);
			
			// chat
			Intent chatIntent = new Intent(CUSTOM_ACTION, Uri.parse(APP_SCHEME + "://chat") );
			chatIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent chatPendingIntent = PendingIntent.getActivity
					(getApplicationContext(), 0, chatIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.footerButtonC, chatPendingIntent);			
			
			
			//refresh widget to show text and listeners
			widgetManager.updateAppWidget(widgetIds, remoteViews); 
			Log.d("UpdateWidget Execute", " onPostExecute finished");
		}
	}
	
	
	public String cleanStoredString(String value){
		String cleaned = value.replace("\n", "");
		cleaned = value.trim();
		return cleaned;
	}
}