package com.todoclaro.miclaroapp;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class WidgetBalanceProvider extends AppWidgetProvider {

	
	@Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.d("WidgetBalanceProvider", "onUpdate()");
        
        Intent intent = new Intent(context.getApplicationContext(), UpdateWidgetBalanceService.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);

        Log.d("WidgetBalanceProvider", "context.startService(intent)");
        
        context.startService(intent);
    }
	
}